/**
 * Created by m.najafi on 10/19/2017.
 */
public class Compte {
    private int solde;


    public Compte(int solde) {

        this.solde = solde;
    }

    public int getSolde() {
        return solde;
    }

    public void ajouter(float somme) throws NonPositiveException {
        if (somme < 0) throw new NonPositiveException("la somme est negatif");
        solde += somme;
    }

    public void retirer(float somme)throws NonPositiveException {
        if (somme< 0) throw new NonPositiveException("La somme est negative");
        if ((solde-somme) < 0) throw new NonPositiveException("le solde ne suffit pas pour retirer");
        solde -= somme;
    }

    public void viremment(Compte compte, float somme) throws NonPositiveException {
        if (somme< 0) throw new NonPositiveException("La somme est negative");
        if ((solde-somme) < 0) throw new NonPositiveException("le solde ne suffit pas pour faire un virement");
        compte.ajouter(somme);
        this.retirer(somme);
    }



}
