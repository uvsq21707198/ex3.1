/**
 * Created by m.najafi on 10/19/2017.
 */



import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CompteTest {

    @Test
    void getSolde() {
        Compte compte = new Compte(100);
        assertEquals(100,compte.getSolde());
    }
    @Test
    void ajouter_une_somme_positive() throws NonPositiveException {
        Compte compte = new Compte(100);
        compte.ajouter(100);
        assertEquals(200,compte.getSolde());
    }

    @Test
    void ajouter_une_somme_negative(){
        Compte compte = new Compte(100);
        try {
            compte.ajouter(-200);
            fail("pas d'exception");
        } catch (NonPositiveException e) {

            assertEquals(100,compte.getSolde());
        }
    }

    @Test
    void retirer_une_somme_negative(){
        Compte compte = new Compte(100);
        try {
            compte.retirer(-200);
            fail("pas d'exception");
        } catch (NonPositiveException e) {
            assertEquals(100,compte.getSolde());

        }


    }
    @Test
    void retirer_une_somme_positif() throws NonPositiveException {
        Compte compte = new Compte(100);
        compte.retirer(50);
        assertEquals(50,compte.getSolde());
    }
    @Test
    void retirer_une_somme_superieur_que_le_solde(){

        Compte compte = new Compte(100);
        try {
            compte.retirer(200);
            fail("pas d'exception");
        } catch (NonPositiveException e) {
            assertEquals(100,compte.getSolde());
        }

    }
    @Test
    void viremment_negative() {
        Compte compte = new Compte(100);
        Compte compte2 = new Compte(20);
        try {
            compte.viremment(compte2,-100);
            fail("pas d'exception");
        } catch (NonPositiveException e) {
            assertEquals(100,compte.getSolde());
            assertEquals(20,compte2.getSolde());
        }

    }
    @Test
    void virement_valid() throws NonPositiveException {
        Compte compte = new Compte(100);
        Compte compte2 = new Compte(20);
        compte.viremment(compte2,50);
        assertEquals(50,compte.getSolde());
        assertEquals(70,compte2.getSolde());

    }
    @Test
    void virment_une_somme_sup_solde(){
        Compte compte = new Compte(100);
        Compte compte2 = new Compte(20);
        try {
            compte.viremment(compte2,200);
            fail("pas d'exception");
        } catch (NonPositiveException e) {
            assertEquals(100,compte.getSolde());
            assertEquals(20,compte2.getSolde());
        }

    }

}