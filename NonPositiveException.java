/**
 * Created by m.najafi on 10/19/2017.
 */

public class NonPositiveException extends Exception {
    public NonPositiveException()
    {
        super();

    }

    public NonPositiveException (String message)
    {
        super (message);
    }

}